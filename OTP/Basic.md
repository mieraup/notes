OTP 
    Rich Abstraction that provides
    - Fault Tolerance

    - Distribution

    - Concurrency

mix new <name> --sup (Generate Supervisor Tree)

GenServers for OTP
    GenServers are generic servers.

    We use specific functions to handle inc, dec, and val


    GenServer.call is synchronous response

    GenServer.cast is async

WE CAN BUILD SELF HEALING SOFTWARE WITHOUT THE COMPLEXITY

Three options for restart

    :permanent - Always restart child
    
    :temporary - The child is never restarted

    :transient - The child is restarted ONLY if it terminates abnormally

Restart strategires
    
    :one_for_one - If child terminates, restarts just that process 
    
    :one_for_all - If a child terminates, supervisor terminates all children and restarts ALL children

    :rest_for_one - If child terminates, supervisor terminates all child processes defined after that one dies. Supervisor restarts all terminated processes


Child Specs
    Cannot have the following since all have same id of InfoSys.Counter

        children = [
      # Starts a worker by calling: InfoSys.Worker.start_link(arg)
      {InfoSys.Counter, 5},
      {InfoSys.Counter, 10},
      {InfoSys.Counter, 15}
    ]

    Do this instead

    childen = [
        ...
        {InfoSys.Counter, 10, id: :some_unique_worker_name_here}
    ]


TODO Leverage in further research of The Project. Current The Project goals are contained for next 90 days. Can adapt after that.