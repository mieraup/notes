If everything went thru a single process for caching, that process would be the bottle neck.

However, Erlang has a service called ETS which allows in memory caching, since it's per node, we do not have to worry about cache sharing or cache synchronization or anything like that.