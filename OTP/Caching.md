All webservers should provide caching of content so that same work isn't done again and again in a short period of time.

We need to do  cache sweeping, clearing out old values when they are no longer needed, otherwise our memeory footprint would continue to grow.