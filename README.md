# notes


Acquired Knowledge
- Cyberpsychology  An introduction to Human Computer Interaction by Kent L. Norman
- Beautiful Code Edited by Andy Oram & Greg Wilson
- Programming Pearls by Jon Bentley
- Code Reading by Diomidis Spinellis
- 97 Things Every Programmer should know Edited by Kevlin Henney
- Object Oriented Programming by Timothy But
- Programming Phoenix >= 1.4 by Chris McCord, Bruce Tate and Jose Valim