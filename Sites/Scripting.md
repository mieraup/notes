Scripts can have async and defer attached like so

<script defer src=".."></script>

defer runs in the order they are listed in the page, AFTER page rendered, then site will load script

async runs whenever it is loaded. It is responsible for doing it only after DOMContentLoaded event is fired.