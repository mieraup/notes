There are four types of testing:

- Unit Test - Function for one layer. NOT method, methods are attached to a class, functions are NOT.

- Integration Test - Different layers working together

- Acceptance Test - Multiple actions working together

- Performance Test - How application performs under load

Stub vs Mock

Stub is a simple standin for more difficult to test code that always returns a predictable result

Mock is a more complicated standin that will fail the test if it isn't treated in the expected way (ie called twice with certain parameters)

Plugable Systems
    For things like testing http connections, we could provide a plugable testing like so

    @http Application.get_env(:info_sys, :wolfram)[:http_client] || :httpc #Falls back to httpc

    This allows stubbing our http client at testing time, while keeping it running fast, not modifying global functions (thereby making tests easier to understand, maintain and extend)