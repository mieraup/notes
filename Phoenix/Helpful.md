####
HINT: it looks like the "do" on line 9 does not have a matching "end"

####
warning: Using Phoenix.ConnTest is deprecated, instead of:

    use Phoenix.ConnTest

do:

    import Plug.Conn
    import Phoenix.ConnTest
