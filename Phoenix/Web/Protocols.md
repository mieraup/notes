When we setup linking between pages we use the Routes.XXX_path(@conn, :action, textmassaged here)
    This is bad because we have to do the textmassaged for EVERY type we use the Routes for that action 

Since routes are part of a protocol, we can redefine the protocol for handling that specific struct. Thereby keeping the change of the protocol in 1 play instead of 1000 places.