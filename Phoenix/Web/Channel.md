Open up a connection to the server and each side just talks back and forth like a regular conversation.

Send and recieve using a dedicated process.

Channel's manage the scaling, state and such. ALL we have to do is three things:

-   Making and breaking connections
-   Sending Messages
-   Recieving Messages


We use a socket which has several layers of abstraction:
    All messages come in a single stream, server responsible for correctly routing them to the correct place
    Everything is provided over a generic transport which allows us to implement our OWN transport but still use the same socket code. 

    This is client abstraction, instead of worrying about which specific way connection is being made, that instead is handled at a lower level of abstraction and we get the socket view which was the message AFTER transportation.  

Channel has three ways to receive events

    - handle_in direct events
    - handle_out Broadcast events 
    - handle_info OTP events (still not fully sure what this is)

Channel holds conversations whereas controllers process requests

Do not forward the raw payload because then you're allowing anyone to send arbitrary messages to anyone else.

Tokens are better then login credential pattern because the user session is tied to a specific protocol (HTTP(s))
    Tokens are transport agnostic