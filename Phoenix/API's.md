We need to have a timeout so that inf the backends do not respond within a certain period of time, we leave without them. 

Caching allows us to fetch the same result in microseconds instead of seconds, this is similar to safari's preload. If the user is going to ask for it, they most likely will ask for it again in the near future.