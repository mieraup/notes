# Seeding
## Purpose of seeding is to allow hardcoding data in a way that is easily changed before deployment or during deployment WITHOUT exposing a web endpoint.

Seeding occurs in the priv/repo/seeds.exs.

Trigger seeding by calling mix run priv/repo/seeds.exs

