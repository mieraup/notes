Testing with tags allows us to run selective tests.

We can marks tests with certain tags which allows us to run the tests we on ON_DEMAND.

Tags accept either keyword list or atoms.

Keyword list is something like @tag someval: test
Whereas atoms are for boolans. @tag :someattom is the same as @tag someattom: true

Running selected tests mix test --only login_as