Run tests by ```mix test```

Can test individual test cases by

```mix test test/path/to/individual/file.ex(:optionalLineStartNumber)```

Testing Hygiene
    Catch exceptions as close as where they SHOULD occur. This allows exceptions thrown elsewhere not to be masked and possibly to false success.