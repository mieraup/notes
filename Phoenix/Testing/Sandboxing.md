Ecto.Adapters.SQL.Sandbox means that after testing is finished all changes are rolled back.

This provides consistency across all testing.