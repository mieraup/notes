This can occur when you have two process tries to access the same resource in the database. (Oversimplification but one that suites our purposes)

To prevent this in testing have unique data like email addresses or usernames generated in the following format:

"value#{System.unique_integer([:positive])}" The #{} for string interprolation.

This can increase tests speeds up to 3x times since there is not as much deadlocks or waiting occuring.