Since Ecto builds on top of SQL databases so it is abstracting databse.

2 Failsafe systems
    Lightweight Failsafe System - Fragement, creating SQL fragements which allow you to do everything that you can and CANNOT do with Ecto macros.

    Heavywieght Failsafe System - Direct access to the database, directly executing SQL statements to the database, bypassing Ecto abstractions but receiving a result that can be used in elixir.

In otherwords, assume that your abstraction is not enough and there will be use cases that you cannot imagine that cannot be done with your macros. That is Lightweight Failsafe System. This system still protects against SQL injection attacks by properly escaping strings.

Then assume that your Lightweight Failsafe System will still fail to do what someone does. Provide raw access to the abstracted resource, this means that you will never disapoint and in the rare cases that someone needs to drop a layer of abstraction they can.