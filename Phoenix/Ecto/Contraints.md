Constraint - Uniqueness or Integrity Constraint
Constraint Error - When Ecto finds problem with insertion or update that violates constraints placed upon the database
Changeset Constraint - Allows Ecto to hold error messages related to constraint issues
Changeset Error MEssages - Human Reabable and Enjoyably. Not disgusting or horrible but beautiful


Constrains can validated using the |> unique_constraints(:fieldsthatmustbeunique)

Remote constraints validated using the |> foreign_key_constraint(Lfiels constrated to)

When deleting a table or values that are linked to another table ie categores in videos linked to table that has all available categories. Delting a category that has videos attached can be tricky, Ecto offeres multiple solutions

There are three options:

:nothing 

:delete_all - All things linked to deleted thing is deleted

:nilify_all - All things linked to deleted thing has that field set to null or nil or whatever absense of data you wish to use

Use in ecto when setting up relationship:
    add :category_id, references(:categories(the schema), on_delete: (:nothing|:delete_all|:nilify_all))

KEEP within these constraints, that is if the database can do the job, let the database do the job. It's optimized for that.


DO NOT SETUP CONTRAITS ON EVERYTHING, that's called nailing a corpse in standing position. Constraints that should be maintained by the application SHOULD be maintained by the application, if those fail let the application crash. There is a bug in your code. Fix it.

ONLY use contraints and changeset errors IFF the user can fix the error (like username and categories)