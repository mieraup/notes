Since we can associate data, by default ecto does not preload associated data.

We can explicity provide that by using preload: [:field1, :field2, ...] in the Repo.one(query, limit: 1, preload: [list])