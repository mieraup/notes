Repo.one - Return one row

from x in someSchema - Reading from the someSchema

x.field == ^varibleInterperloated 
                |> Means that this value was matched here was created somewhere else

Can stack queries up

user_count = from uORANYOTHERVALUE in User, select: count(uOrANYOTHERVALUE.id)

then later we can add the statement user_count to another statement

other_search = from xOrAnyOthervalue in user_count, select: TARGET

The other_search is using the results from user_count to power it's search. How cool and modular and stackable is that?

!!! Fails
username = 123
Repo.all(from u in User, where: u.username == ^username)

Because we cannot cast 123 to a string which is what the username needs to be.

Query API

Comparison ==,!=,<=, =>, < ,>
Boolean and, or, not
Inclusion operator in
Search function like, ilike
    like is case sensitive, ilike is case INSENSITIVe
    From Humane UI we know that typing lowercase means that it COULD be uppercase but typing uppercase means it IS uppercase
Null check: is_nil
Aggregates: count, avg, sum, min, max
Data/time intervals: datetime_add, date_add
General: fragment, field, type

## Interpreting Queries

### Example OR and Ilike
Repo.one from u in User,       
...(15)> select: count(u.id),           
...(15)> where: ilike(u.username, "j%") or
...(15)> ilike(u.username, "c%")  

QUERY OK source="users" db=3.0ms queue=1.1ms idle=1500.2ms
SELECT count(u0."id") FROM "users" AS u0 WHERE ((u0."username" ILIKE 'j%') OR (u0."username" ILIKE 'c%')) []

Select only one row (if there is no row or two rows, throw an error please) that just returns the id of the user that starts with either j or c.


## Questions 

Is short circuit logic or evaluation? Yes.