Sometime the best way to fix is to restart. Phoenix makes it easy because database setup and migration are completely automated which makes making this decision a no brainer.

```
psql -U username

DROP DATABASE name;

mix ecto.create

mix ecto.migrate
```

