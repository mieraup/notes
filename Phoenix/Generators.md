# Generators

- phx.gen.schema.html Controller, View, Template for Frontend. Multimedia context, schema and migration
- phx.gen.schema.json Same shit except for JSON endpoints
- phx.gen.context Create context and such WITHOUT exposing to frontent
- phx.gen.schema Create a resource without defining any functions
- ecto.gen.migration  Create migration after schema and context ALREADY created